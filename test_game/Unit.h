#pragma once
#include "FrontEnd.h"
#include <memory>
struct Stats
{
	unsigned int health, mana, attack, armor;
};

class Unit
{
protected:
	Stats m_stats;
public:
	virtual int Turn() = 0;
	Unit(unsigned int health, unsigned int mana, unsigned int attack, unsigned int armor) :
		m_stats({ health, mana, attack, armor }){};

	virtual ~Unit();
};