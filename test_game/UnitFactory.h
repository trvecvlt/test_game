#pragma once
#include <memory>
#include "Unit.h"

enum unitTypes{PLAYER, CREATURE};
class UnitFactory
{
public:
	static std::unique_ptr < Unit > CreateUnit(unitTypes type, unsigned int health = 100, unsigned int mana = 100, unsigned int attack = 10, unsigned int armor = 40);
};

