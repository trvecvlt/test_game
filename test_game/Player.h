#pragma once
#include "Unit.h"
#include <memory>
class Player :
	public Unit
{
public:
	Player(unsigned int health, unsigned int mana, unsigned int attack, unsigned int armor) :
		Unit(health, mana, attack, armor ){};
	virtual int Turn();
	virtual ~Player();
};

