#include "stdafx.h"
#include "FrontEnd.h"
#define BUFSIZE 4096


using namespace std;


void SampleFrontEnd::ReciveAnswer()
{
	
}

string SampleFrontEnd::SendCommand(string command)
{

	HANDLE hPipe;
	char chReadBuf[BUFSIZE];
	BOOL fSuccess;
	DWORD cbRead, dwMode;
	LPTSTR lpszPipename = TEXT("\\\\.\\pipe\\testpipe");

	while (true)
	{
		hPipe = CreateFile(
			lpszPipename,   // pipe name 
			GENERIC_READ |  // read and write access 
			GENERIC_WRITE,
			0,              // no sharing 
			NULL,           // default security attributes
			OPEN_EXISTING,  // opens existing pipe 
			0,              // default attributes 
			NULL);          // no template file 

		// Break if the pipe handle is valid. 
		if (hPipe != INVALID_HANDLE_VALUE)
			break;

		// Exit if an error other than ERROR_PIPE_BUSY occurs. 
		if (GetLastError() != ERROR_PIPE_BUSY)
		{
			cout << "Could not open pipe with server\n";
			return "";
		}

		// All pipe instances are busy, so wait for 20 seconds. 
		if (!WaitNamedPipe(lpszPipename, 20000))
		{
			cout << "Could not open pipe with server, all pipes are busy\n";
			return "";
		}
	}
	dwMode = PIPE_READMODE_MESSAGE;
	fSuccess = SetNamedPipeHandleState(
		hPipe,    // pipe handle 
		&dwMode,  // new pipe mode 
		NULL,     // don't set maximum bytes 
		NULL);    // don't set maximum time 
	if (!fSuccess)
	{
		cout << "Pipe is brocken. Try again later.\n";
		return "";
	}

	char sendline[BUFSIZE];
	strcpy_s(sendline, BUFSIZE, command.c_str());
	fSuccess = TransactNamedPipe(
		hPipe,                  // pipe handle 
		sendline,              // message to server
		command.length() + 1, // message length 
		chReadBuf,              // buffer to receive reply
		BUFSIZE,  // size of read buffer
		&cbRead,                // bytes read
		NULL);                  // not overlapped 

	if (!fSuccess && (GetLastError() != ERROR_MORE_DATA))
	{
		cout <<"Pipe is brocken. Try again later.\n";
		return "";
	}

	string res = "";

	while (1)
	{
		_tprintf(TEXT("%s\n"), chReadBuf);

		// Break if TransactNamedPipe or ReadFile is successful
		if (fSuccess)
			break;

		// Read from the pipe if there is more data in the message.
		fSuccess = ReadFile(
			hPipe,      // pipe handle 
			chReadBuf,  // buffer to receive reply 
			BUFSIZE*sizeof(char),  // size of buffer 
			&cbRead,  // number of bytes read 
			NULL);    // not overlapped 

		// Exit if an error other than ERROR_MORE_DATA occurs.
		if (!fSuccess && (GetLastError() != ERROR_MORE_DATA))
			break;
		else res += chReadBuf;
	}

	CloseHandle(hPipe);
	return res;
}