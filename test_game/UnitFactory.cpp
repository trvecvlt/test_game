#include "stdafx.h"
#include "UnitFactory.h"
#include "Creature.h"
#include "Player.h"

using namespace std;

unique_ptr <Unit> UnitFactory::CreateUnit(unitTypes type, unsigned int health, unsigned int mana, unsigned int attack, unsigned int armor)
{ 
	switch (type)
	{
	case PLAYER:
		return unique_ptr <Unit>(new Player(health, mana, attack, armor));
	case CREATURE:
		return unique_ptr <Unit>(new Creature(health, mana, attack, armor));
	}
}


