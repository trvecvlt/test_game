#pragma once
#include <string>
#include <windows.h> 
#include <stdio.h>
#include <conio.h>
#include <tchar.h>
#include <iostream>

class SampleFrontEnd
{
public:
	static std::string SendCommand(std::string command);
	static void ReciveAnswer();
private:
	SampleFrontEnd();
	virtual ~SampleFrontEnd();
};