// test_game.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <string>
#include <iostream>
#include <memory>
#include "Unit.h"
#include "UnitFactory.h"

using namespace std;

void usage()
{
	cout << "Test game accepts following commands:\n";
	cout << "Attack - attack enemy in melee combat\n";
	cout << endl;
}

void main_loop(unique_ptr <Unit> player)
{
	while (player->Turn());
}

int _tmain(int argc, _TCHAR* argv[])
{
	auto pl = UnitFactory::CreateUnit(PLAYER);
	main_loop(move(pl));
	return 0;
}

