#pragma once
#include "Unit.h"
class Creature :
	public Unit
{
	unsigned int m_id;
	virtual int Turn()override;
public:
	Creature(unsigned int health, unsigned int mana, unsigned int attack, unsigned int armor) :
		Unit(health, mana, attack, armor){};
	virtual ~Creature();
};

